const frameModule = require("tns-core-modules/ui/frame");
const observable_1 = require("tns-core-modules/data/observable");
const appSettings = require("tns-core-modules/application-settings");
const view = require("tns-core-modules/ui/core/view");

var ValueList = (function () {
    function ValueList(array) {
        this._array = array;
    }
    Object.defineProperty(ValueList.prototype, "length", {
        get: function () { return this._array.length; },
        enumerable: true,
        configurable: true
    });
    ValueList.prototype.getItem = function (index) {
        return this.getText(index);
    };
    ValueList.prototype.getText = function (index) {
        if (index < 0
            || index >= this._array.length) {
            return "";
        }
        return this._array[index].display;
    };
    ValueList.prototype.getValue = function (index) {
        if (index < 0
            || index >= this._array.length) {
            return null;
        }
        return this._array[index].value;
    };
    ValueList.prototype.getIndex = function (value) {
        var loop;
        for (loop = 0; loop < this._array.length; loop++) {
            if (this.getValue(loop) == value) {
                return loop;
            }
        }
        return -1;
    };
    return ValueList;
})();

function onNavigatingTo(args) {
    const page = args.object;
    page.bindingContext = page.navigationContext;
}

function pageLoaded(args) {
    // Get the event sender
    var page = args.object;

    console.log('loaded');

    var viewModel = new observable_1.Observable();

    var value = parseFloat(appSettings.getNumber("selectedEvent"));
    var index = parseFloat(appSettings.getNumber("selectedIndex"));

    console.log(value);

    var dd = view.getViewById(page, "dd");
    var itemSource = new ValueList(page.navigationContext.items);
    dd.items = itemSource;
    dd.selectedIndex = itemSource.getIndex(0);

    viewModel.set("selectedIndex", index); // value

    console.log(viewModel);

    page.bindingContext = viewModel;

}

exports.onNavigatingTo = onNavigatingTo;
exports.pageLoaded = pageLoaded;

exports.onchange = function(args) {
    var page = args.object;
    var dd = view.getViewById(page, "dd");
    var selectedValue = dd.items.getValue(dd.selectedIndex);
    var selectedIndex = dd.items.getIndex(selectedValue);
    appSettings.setNumber("selectedEvent", selectedValue);
    appSettings.setNumber("selectedIndex", selectedIndex);
}
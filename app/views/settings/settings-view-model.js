const observableModule = require("tns-core-modules/data/observable");

function SettingsViewModel() {
    const viewModel = observableModule.fromObject({});

    return viewModel;
}

module.exports = SettingsViewModel;

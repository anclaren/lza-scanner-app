const scannerModel = require("./../../scanner");
const settingsModel = require("./../../module/settings");
const frameModule = require("tns-core-modules/ui/frame");
const Observable = require("tns-core-modules/data/observable").Observable;


function onNavigatingTo(args) {
    const page = args.object;
}

function pageLoaded(args) {
    const page = args.object;
    page.bindingContext = new scannerModel.Scanner(page);
    page.bindingContext.settings = new settingsModel.Settings();
    page.bindingContext.settings.items = page.bindingContext.settings.getEvent();
}

exports.onNavigatingTo = onNavigatingTo;
exports.pageLoaded = pageLoaded;

exports.onSettings = function(args) {
    const page = args.object;
    const topmost = frameModule.topmost();
    //console.log(page.bindingContext.settings.items[0].display);
    const navigationEntry = {
        moduleName: "views/settings/settings-page",
        context: {items: page.bindingContext.settings.items},
        animated: true
    };

    topmost.navigate(navigationEntry);
};
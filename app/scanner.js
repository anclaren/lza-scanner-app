var observable_1 = require("tns-core-modules/data/observable");
var BarcodeScanner = require("nativescript-barcodescanner").BarcodeScanner;
var appSettings = require("tns-core-modules/application-settings");
var http = require("tns-core-modules/http");
var dialogs_1 = require("tns-core-modules/ui/dialogs");
var view = require("tns-core-modules/ui/core/view");
var frame = require("tns-core-modules/ui/frame");
var Observable = require("tns-core-modules/data/observable").Observable;
var D = {};

D.closure = function( $this, fn, var1 ){
    if( typeof fn != 'function' ){
        throw new Error('Closure fn not function');
    }
    var args = [];
    for( var i = 2; i < arguments.length; ++ i ){
        args.push( arguments[i] );
    }
    return function(){
        $this = $this || this;
        var a = [];
        for( var i = 0; i < arguments.length; ++ i ){
            a.push( arguments[i] );
        }
        return fn.apply( $this, args.concat(a) );
    };
};
var Scanner = (function (_super) {
    __extends(Scanner, _super);
    function Scanner(page) {
        this.barcodescanner = new BarcodeScanner();
        this.messageLabel = null;
        this.page = page;
        this.test = 'asdasdas123123';
        this.myItems = [

        ];
        _super.call(this);
        console.log('Super Call');
        console.log(this.page);
    };
    Scanner.prototype.doTest = function () {
        this.barcodescanner.scan({
            formats: "QR_CODE,PDF_417,EAN_13,EAN_8",
            showFlipCameraButton: false,
            preferFrontCamera: false,
            showTorchButton: false,
            beepOnScan: true,
            torchOn: false,
            closeCallback: function () {
                console.log("Scanner closed");
            },
            openSettingsIfPermissionWasPreviouslyDenied: true
        }).then(
            this.scanResult,
            function(error) {
                alert("Nav noskenēts");
            }
        );
    };
    Scanner.prototype.getPage = function () {
      console.log(this);
    };
    Scanner.prototype.scanResult = function (result) {
        console.log('Scanner results goeas here');
        this.result = result;
        this.page = frame.topmost().currentPage;
        console.log(this.page);
        console.log(this.result.text);
        var value = parseFloat(appSettings.getNumber("selectedEvent"));
        http.request({
            url: "https://lza.registration.lv/api/events/scan?event_id=" + value + "&id="+result.text+"",
            method: "GET",
            headers: { "Content-Type": "application/json" },
        }).then(
            function (response) {
                var results = response.content.toJSON();
                console.log(results.status);

                // Set title
                this.page.getViewById("myText").text = results.title;

                this.page.getViewById("info2").text = results.msg;

                // Set status
                if (results.status === true) {
                    this.page.getViewById("info").text = "Ir Reģistrēts";
                } else {
                    this.page.getViewById("info").text = "Lietotājs nav atrasts";
                    this.page.getViewById("list").items = [];
                    this.page.getViewById("infoArrived").text = '';
                    this.page.getViewById("infoTimeTotal").text = '';
                }

                this.myItems = [];
                var history = JSON.parse(results.history);
                for (var i in history) {
                    this.myItems.push({ status: history[i].status, time: history[i].time });
                }

                this.page.getViewById("list").items = this.myItems;

                this.page.getViewById("infoArrived").text = "Ieradās: " + results.arrived;
                this.page.getViewById("infoTimeTotal").text = "Kopā pavadītais laiks:  " + results.total_time;
            },
            function (e) {
                console.log(e);
            }
        );
    };
    Scanner.prototype.sendData = function(id) {
        alert(id);
    };
    return Scanner;
}(observable_1.Observable));
exports.Scanner = Scanner;
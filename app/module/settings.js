var observable_1 = require("tns-core-modules/data/observable");
var appSettings = require("tns-core-modules/application-settings");
var http = require("tns-core-modules/http");
var Settings = (function (_super) {
    __extends(Settings, _super);
    function Settings() {
        console.log('Settings model init');
        _super.call(this);
        this.selected = 0;
        this.event_list = [{
            id: 1,
            title: 'Holly shiiiiit'
        }];
    }
    Settings.prototype.getEvent = function () {
        var list = [];
        http.request({ url: "https://lza.registration.lv/api/events", method: "GET" }).then(function (response) {
            var obj = response.content.toJSON();
            for (var i in obj) {
                list.push({
                    value: obj[i].id,
                    display: obj[i].title
                });
            }
        }, function (e) {
            //// Argument (e) is Error!
        });

        return list;
    };
    return Settings;
}(observable_1.Observable));
exports.Settings = Settings;